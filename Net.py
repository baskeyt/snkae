import torch
relu = torch.nn.ReLU()

# Residual block with 2 layers
# input -> convolution -> batchnorm -> relu -> convolution -> batchnorm -> relu + skip connection

class ResBlock(torch.nn.Module):
    def __init__(self, n, features):
        super(ResBlock, self).__init__()
        self.conv1 = torch.nn.Conv2d(features, features, (3, 3,), padding=1)
        self.conv2 = torch.nn.Conv2d(features, features, (3, 3,), padding=1)

        self.bn1 = torch.nn.BatchNorm2d(features)
        self.bn2 = torch.nn.BatchNorm2d(features)
    def forward(self, input_):
        return relu(self.bn2(self.conv2(relu(self.bn1(self.conv1(input_)))))) + input_

# The actual network, made of residual blocks
# First it takes the snake board, which has 5 features for head1, body1, food, body2, head1
# Then it makes it have the standard number of features by applying 2 inital convolutions, no skip
# Then it runs it through however many residual blocks
# Finally this data is used as input for the actor and critic 'sub-networks (to be added)', a la AlphaZero

class Net(torch.nn.Module):
    def __init__(self, n, features, blocks):
        super(Net, self).__init__()
        self.res_blocks = torch.nn.ModuleList([ResBlock(n, features) for _ in range(blocks)])

        self.conv1 = torch.nn.Conv2d(5, features, (3, 3), padding=1)
        self.conv2 = torch.nn.Conv2d(features, features, (3, 3), padding=1)

        self.bn1 = torch.nn.BatchNorm2d(features)
        self.bn2 = torch.nn.BatchNorm2d(features)
    def forward(self, input_):
        x = relu(self.bn2(self.conv2(relu(self.bn1(self.conv1(input_))))))
        for res_block in self.res_blocks:
            x = res_block(x)
        return x

x = torch.rand([2**10, 5, 16, 16])

test = Net(16, 16, 3)
print(test(x).shape)